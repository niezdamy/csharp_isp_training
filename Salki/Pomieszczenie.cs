﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salki
{
    abstract class Pomieszczenie : IOtwarty
    {
        public int Piętro;
        public string Budynek;
        public bool CzyOtwarte;

        virtual public string SprawdzOtwartosc()
        {
            if (CzyOtwarte) return "Otwarte, zapraszam";
            else return "Zamknięte";
        }

        public virtual void CzyOtwarty()
        {
            Console.WriteLine(CzyOtwarte ? 
                "Jestem otwartym pomieszczeniem :) "
                : "Jestem zamkniętym pomieszczeniem :( "
                );
        }
    }
}
