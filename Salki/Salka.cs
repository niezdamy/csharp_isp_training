﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salki
{
    class Salka : Pomieszczenie, IPrzedstawienie
    {
        public string Nazwa;
        public int IloscOsob;
        public bool Telewizor;
        public bool CzyDziala;


        public void PrzedstawSie()
        {
            Console.WriteLine("Jestem Salką, nazywam się: " + Nazwa);
        }

    }
}
