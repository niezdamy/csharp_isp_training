﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Salki
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //Generowanie list danych testowych
            var listaSalek = GenerujSalki();
            var listaStrefRelaksu = GenerujStrefyRelaksu();

            //Przedstaw salki z piętra drugiego

            //listaSalek.FirstOrDefault(x => x.Piętro == 2).PrzedstawSie();

            //Console.WriteLine();

            ////Posortowane otwarte strefy relaksu
            //var listaOtwartychStref =     
            //    listaStrefRelaksu.Where(x => x.CzyOtwarte == true)
            //    .OrderBy(y => y.Piętro);

            //////wyświetlenie otwartych stref relaksu
            //foreach (var strefa in listaOtwartychStref)
            //{
            //    strefa.PrzedstawSie();
            //}
            //Console.WriteLine();

            ////połączenie salek oraz stref relaksu w jedną listę za pomocą interfejsu IOtwarty
            ////wyświetlenie metody "CzyOtwarty" dla każdego obiektu w liście
            ////c=>(IOtwarty)c - rzutowanie obiektu na typ IOtwarty

            var listaOtwartych = new List<IOtwarty>();

            listaOtwartych.AddRange(listaSalek.Select(c => (IOtwarty)c));
            listaOtwartych.AddRange(listaStrefRelaksu.Select(c => (IOtwarty)c));

            listaOtwartych.ForEach(c => c.CzyOtwarty());

            Console.ReadKey();
        }

        private static List<Salka> GenerujSalki()
        {
            var listaSalek = new List<Salka>();

            var salka1 = new Salka
            {
                Nazwa = "Death Star",
                IloscOsob = 12,
                Budynek = "Stern",
                CzyDziala = true,
                CzyOtwarte = false,
                Piętro = 6,
                Telewizor = false
            };

            var salka2 = new Salka
            {
                Nazwa = "Hiszpańska Inkwizycja",
                IloscOsob = 12,
                Budynek = "GPP3",
                CzyDziala = true,
                CzyOtwarte = false,
                Piętro = 2,
                Telewizor = false
            };
            listaSalek.Add(salka1);
            listaSalek.Add(salka2);


            return listaSalek;
        }

        private static List<StrefaRelaksu> GenerujStrefyRelaksu()
        {
            var listaStrefRelaksu = new List<StrefaRelaksu>();


            var strefa1 = new StrefaRelaksu
            {
                Nazwa = "Pinglownia",
                Budynek = "Stern",
                CzyOtwarte = true,
                Piętro = 6,
                Atrakcje = "Ping pong"
            };

            var strefa2 = new StrefaRelaksu
            {
                Nazwa = "Piłkarzyki dobre",
                Budynek = "Stern",
                CzyOtwarte = false,
                Piętro = 4,
                Atrakcje = "Piłkarzyki"
            };

            var strefa3 = new StrefaRelaksu
            {
                Nazwa = "Piłkarzyki słabe",
                Budynek = "Stern",
                CzyOtwarte = true,
                Piętro = 1,
                Atrakcje = "Piłkarzyki"
            };

            listaStrefRelaksu.Add(strefa1);
            listaStrefRelaksu.Add(strefa2);
            listaStrefRelaksu.Add(strefa3);

            return listaStrefRelaksu;
        }
    }
}