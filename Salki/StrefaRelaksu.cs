﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Salki
{
    class StrefaRelaksu : Pomieszczenie, IPrzedstawienie
    {
        public string Nazwa;
        public string Atrakcje;
        public bool CzyDziala;

       public override void CzyOtwarty()
        {
            if (CzyDziala) Console.WriteLine("Jestem otwartą strefą relaksu o nazwie: " + Nazwa);
            else Console.WriteLine("Jestem zamkniętą strefą relaksu o nazwie: " + Nazwa);
        }

        public void PrzedstawSie()
        {
             Console.WriteLine("Jestem Strefą Relaksu, nazywam się: " + Nazwa);
        }

        public void PrzedstawSie(string imie)
        {
            //return "Jestem pomieszczeniem, nazywam się " + Nazwa;
            Console.WriteLine("Jestem Strefą Relaksu, nazywam się: " + Nazwa + " Witaj " + imie);
        }
    }
}
